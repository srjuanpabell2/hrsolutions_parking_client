import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule, LocationStrategy, HashLocationStrategy } from '@angular/common';

//Rutas
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

//Animaciones
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

//Toast (Notificaciones)
import { ToastsManager, ToastOptions, ToastModule } from 'ng2-toastr/ng2-toastr';

//Historicos de ingresos y salidas
import { HistoricalsModule } from './modules/Historicals/historicals.module';

//Tarifas
import { RatesModule } from './modules/Rates/rates.module';

//Vehiculos
import { VehiclesModule } from './modules/Vehicles/vehicles.module';
import { HttpClientModule } from '@angular/common/http';

//General
import { GeneralModule } from './modules/General/general.module';

//HTTPClient

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    GeneralModule,
    BrowserAnimationsModule,
    CommonModule,
    HistoricalsModule,
    RatesModule,
    VehiclesModule,
    ToastModule.forRoot()
  ],
  providers: [
    ToastsManager,
    ToastOptions,
    {provide: LocationStrategy, useClass: HashLocationStrategy}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
