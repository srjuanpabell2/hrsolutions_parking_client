import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HistoricalsListComponent } from './modules/Historicals/components/historicals-list/historicals-list.component';
import { HistoricalsDetailComponent } from './modules/Historicals/components/historicals-detail/historicals-detail.component';
import { VehicleTypesCreateComponent } from './modules/Vehicles/components/vehicle-types-create/vehicle-types-create.component';
import { VehicleTypesListComponent } from './modules/Vehicles/components/vehicle-types-list/vehicle-types-list.component';
import { VehicleListComponent } from './modules/Vehicles/components/vehicle-list/vehicle-list.component';
import { VehicleCreateComponent } from './modules/Vehicles/components/vehicle-create/vehicle-create.component';
import { RatesCreateComponent } from './modules/Rates/components/rates-create/rates-create.component';
import { VehicleTypesDeleteComponent } from './modules/Vehicles/components/vehicle-types-delete/vehicle-types-delete.component';
import { HistoricalsCreateComponent } from './modules/Historicals/components/historicals-create/historicals-create.component';

const routes: Routes = [
  {path: 'historical_create', component: HistoricalsCreateComponent},
  {path: 'historicals', component: HistoricalsListComponent},
  {path: 'historicals/:id', component: HistoricalsListComponent},
  {path: 'generate-ticket/:historical_id', component: HistoricalsDetailComponent},
  {path: 'vehicle_types', component: VehicleTypesListComponent},
  {path: 'vehicle_types_create', component: VehicleTypesCreateComponent},
  {path: 'vehicle_types_delete/:vehicle_type_id', component: VehicleTypesDeleteComponent},
  {path: 'vehicles', component: VehicleListComponent},
  {path: 'vehicle_create', component: VehicleCreateComponent},
  {path: 'rates_create', component: RatesCreateComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
