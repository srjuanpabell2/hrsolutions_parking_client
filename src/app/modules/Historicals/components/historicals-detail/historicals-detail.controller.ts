import { Injectable } from "@angular/core";
import { HistoricalsDetailComponent } from "./historicals-detail.component";
import { HistoricalResource } from "../../resources/historicals.resource";
import * as moment from 'moment';
import { Historical } from "../../models/historical.model";

@Injectable()

export class HistoricalsDetailController {

    component: HistoricalsDetailComponent;

    constructor(private historicalResource: HistoricalResource) { }

    setComponent(component: HistoricalsDetailComponent) {
        this.component = component;
    }

    getHistorical() {
        this.historicalResource.setEntity('historicals');
        this.historicalResource.get(this.component.getHistoricalId()).subscribe((historical: any) => {
            this.component.setHistorical(historical.data);
            this.calculateRate();
        }, (err) => {
            console.log(err.error.message);
        });
    }

    calculateRate() {
        let total_minutes = this.calculateTotalMinutes();
        this.component.setMinutes(total_minutes);
        let historical: Historical = new Historical(this.component.getHistorical());
        historical.total_value = historical.vehicle.vehicle_type.rate.minute_value * total_minutes;
        historical.end_time = this.component.getEndTime();
        this.component.setHistorical(historical);
    }

    calculateTotalMinutes() {
        var start = new Date(this.component.getHistorical().start_time);
        var end = new Date(this.component.getEndTime());
        var start_time = moment(start, "YYYY-MM-DD HH:mm:ss");
        var end_time = moment(end, "YYYY-MM-DD HH:mm:ss");
        this.component.setEndTime
        let total_minutes = end_time.diff(start_time, 'm');
        return total_minutes;
    }

    onGenerate() {
        this.historicalResource.setEntity('historicals');
        let historical = this.component.getHistorical();
        this.historicalResource.update(historical.id, {
            end_time: this.component.getEndTime(),
            total_value: historical.total_value,
            vehicle_id: historical.vehicle_id
        }).subscribe(() => {
            this.component.goToHistoricals();
        }, (err) => {
            console.log(err.error.message);
        })
    }

}