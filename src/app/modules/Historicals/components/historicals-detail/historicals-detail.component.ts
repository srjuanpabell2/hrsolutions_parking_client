import { Component, OnInit } from '@angular/core';
import { HistoricalsDetailController } from './historicals-detail.controller';
import { Router, ActivatedRoute } from '@angular/router';
import { Historical } from '../../models/historical.model';
import { VehicleType } from '../../../Vehicles/models/vehicle_type.model';
import { DateAdapter } from '@angular/material';

@Component({
  selector: 'app-historicals-detail',
  templateUrl: './historicals-detail.component.html',
  styleUrls: ['./historicals-detail.component.css']
})
export class HistoricalsDetailComponent implements OnInit {

  private historical_id: number = this.route.snapshot.params['historical_id'];
  private historical: Historical = new Historical({
    vehicle: {identification: '', vehicle_type: { rate: {minute_value: 0}}}
  });
  private total_minutes: number;
  private end_time: string;

  constructor(private controller: HistoricalsDetailController,
              private route: ActivatedRoute,
              private router: Router) { 
                this.controller.setComponent(this);
                
  }

  ngOnInit() {
    this.setEndTime();
    this.controller.getHistorical();
  }

  onGenerate() {
    this.controller.onGenerate();
  }

  goToHistoricals() {
    this.router.navigateByUrl('historicals');
  }

  getHistorical() {
    return this.historical;
  }

  setHistorical(historical: Historical) {
    this.historical = historical;
  }

  getHistoricalId() {
    return this.historical_id;
  }

  setHistoricalId(historical_id: number) {
    this.historical_id = historical_id;
  }

  getEndTime() {
    return this.end_time;
  }

  setEndTime() {
    var date = new Date();
    var new_date = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    this.validateDate(new_date);
  }

  getMinutes() {
    return this.total_minutes;
  }

  setMinutes(total_minutes: number) {
    this.total_minutes = total_minutes;
  }

  validateDate(new_date: string) {
    if (new_date.substr(6,1) == '-') {
       this.end_time = new_date.substring(0,5) + '0' + new_date.substr(5);
    } else {
      this.end_time = new_date;
    }
  }

}
