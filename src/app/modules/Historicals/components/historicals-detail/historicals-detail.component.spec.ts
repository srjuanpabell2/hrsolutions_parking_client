import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricalsDetailComponent } from './historicals-detail.component';

describe('HistoricalsDetailComponent', () => {
  let component: HistoricalsDetailComponent;
  let fixture: ComponentFixture<HistoricalsDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoricalsDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoricalsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
