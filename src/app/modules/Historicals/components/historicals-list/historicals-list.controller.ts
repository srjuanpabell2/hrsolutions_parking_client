import { Injectable } from "@angular/core";
import { HistoricalsListComponent } from "./historicals-list.component";
import { HistoricalResource } from "../../resources/historicals.resource";
import { StorageService } from "../../../General/services/storage.service";
import { Historical } from "../../models/historical.model";

@Injectable()

export class HistoricalsListController {

    private component: HistoricalsListComponent;

    constructor(private historicalResource: HistoricalResource,
        private storageService: StorageService) { }

    setComponent(component: HistoricalsListComponent) {
        this.component = component;
    }

    getHistoricals() {
        this.getHistoricalsParking();
    }

    getHistoricalsBilled() {
        this.historicalResource.setEntity("historicals");
        this.historicalResource.all().subscribe((historicals: any) => {
            let array = new Array<Historical>();
            for(let historical of historicals.data.data) {
                if (historical.total_value > 0) {
                    console.log(true);
                    array.push(historical);
                } else {
                }
            }
            this.component.setHistoricals(array);
        }, (err) => {
            console.log(err.error.message);
        });
    }

    getHistoricalsParking() {
        this.historicalResource.setEntity("historicals");
        this.historicalResource.all().subscribe((historicals: any) => {
            let array = new Array<Historical>();
            for(let historical of historicals.data.data) {
                if (historical.total_value > 0) {

                } else {
                    console.log(false);
                    array.push(historical);
                }
            }
            this.component.setHistoricals(array);
        }, (err) => {
            console.log(err.error.message);
        });
    }

}