import { Component, OnInit } from '@angular/core';
import { HistoricalsListController } from './historicals-list.controller';
import { Historical } from '../../models/historical.model';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-historicals-list',
  templateUrl: './historicals-list.component.html',
  styleUrls: ['./historicals-list.component.css']
})
export class HistoricalsListComponent implements OnInit {

  private page = 1;
  private historicals: Historical[];

  constructor(private controller: HistoricalsListController,
    private router: Router,
    private route: ActivatedRoute) {
    this.controller.setComponent(this);
  }

  ngOnInit() {
    this.controller.getHistoricals();
  }

  getPage() {
    return this.page;
  }

  setPage(page: number) {
    this.page = page;
  }

  getHistoricals() {
    return this.historicals;
  }

  setHistoricals(historicals: Historical[]) {
    this.historicals = historicals;
  }

  onGoToGenerateTicket(historical_id: number) {
    this.router.navigateByUrl('generate-ticket/' + historical_id);
  }

  validateIfIsInParking(historical: Historical) {
    if (historical.total_value != 0) {
      return false;
    } else {
      return true;
    }
  }

  getHistoricalsBilled() {
    this.controller.getHistoricalsBilled();
  }

  getHistoricalsParking() {
    this.controller.getHistoricalsParking();
  }

}
