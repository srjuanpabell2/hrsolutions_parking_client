import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricalsListComponent } from './historicals-list.component';

describe('HistoricalsListComponent', () => {
  let component: HistoricalsListComponent;
  let fixture: ComponentFixture<HistoricalsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoricalsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoricalsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
