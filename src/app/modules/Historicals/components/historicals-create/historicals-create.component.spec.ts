import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricalsCreateComponent } from './historicals-create.component';

describe('HistoricalsCreateComponent', () => {
  let component: HistoricalsCreateComponent;
  let fixture: ComponentFixture<HistoricalsCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoricalsCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoricalsCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
