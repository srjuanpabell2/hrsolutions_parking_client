import { Injectable } from "@angular/core";
import { HistoricalsCreateComponent } from "./historicals-create.component";
import { VehiclesResource } from "../../../Vehicles/resources/vehicles.resource";
import { Vehicle } from "../../../Vehicles/models/vehicle.model";
import { HistoricalResource } from "../../resources/historicals.resource";
import { Historical } from "../../models/historical.model";

@Injectable()

export class HistoricarslCreateController {

    component: HistoricalsCreateComponent;
    start_time: string;
    validate: boolean = true;

    constructor(private vehiclesResource: VehiclesResource,
        private historicalResource: HistoricalResource) { }

    setComponent(component: HistoricalsCreateComponent) {
        this.component = component;
    }

    getVehicles() {
        this.vehiclesResource.setEntity('vehicles');
        this.vehiclesResource.all().subscribe((vehicles: any) => {
            let array = new Array<Vehicle>();
            this.component.setVehicles(vehicles.data.data);
        }, (err) => {
            console.log(err.error.message);
        });
    }

    onCreate() {
        let historical = this.prepareHistorical();
        this.historicalResource.setEntity('historicals');
            this.historicalResource.store(this.prepareHistorical()).subscribe(() => {
                this.component.goToHistoricals();
            }, (err) => {
                this.component.showError(err.error.message);
            });
    }

    prepareHistorical() {
        let historical = new Historical();
        let form = this.component.getCreateForm().value;
        historical.comments = form.comments;
        var date = new Date();
        var new_date = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
        this.validateDate(new_date);
        historical.start_time = this.start_time;
        historical.end_time = this.start_time;
        historical.vehicle_id = Number(form.vehicle_id);
        historical.total_value = 0;
        return historical;
    }

    validateDate(new_date: string) {
        if (new_date.substr(6,1) == '-') {
           this.start_time = new_date.substring(0,5) + '0' + new_date.substr(5);
        } else {
          this.start_time = new_date;
        }
      }

}