import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { HistoricarslCreateController } from './historicals-create.controller';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Vehicle } from '../../../Vehicles/models/vehicle.model';
import { ToastsManager } from 'ng2-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-historicals-create',
  templateUrl: './historicals-create.component.html',
  styleUrls: ['./historicals-create.component.css']
})
export class HistoricalsCreateComponent implements OnInit {

  private createForm: FormGroup;
  private vehicles: Vehicle[] = new Array<Vehicle>();

  constructor(private controller: HistoricarslCreateController,
              private toastr: ToastsManager,
              private vcr: ViewContainerRef,
              private router: Router) { 
    this.controller.setComponent(this);
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.createForm = new FormGroup({
      comments: new FormControl(null , [Validators.required]),
      vehicle_id: new FormControl(null, [Validators.required]),
      start_time: new FormControl(null, Validators.required)
    });
    this.controller.getVehicles();
  }

  getVehicles() {
    return this.vehicles;
  }

  setVehicles(vehicles) {
    this.vehicles = vehicles;
  }

  getCreateForm() {
    return this.createForm;
  }

  onCreate() {
      this.controller.onCreate();
  }

  showSuccess(msg) {
    this.toastr.success(msg);
  }

  showError(msg) {
    this.toastr.error(msg);
  }

  goToHistoricals() {
      this.router.navigateByUrl('historicals');
  }

}
