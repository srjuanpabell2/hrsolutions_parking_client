import { Resource } from "../../General/resources/base.resource";
import { Historical } from "../models/historical.model";

export class HistoricalResource extends Resource {

    protected map(data) {
        return new Historical(data);
    }
}