import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../app.material.module';
import { HistoricalsListComponent } from './components/historicals-list/historicals-list.component';
import { HistoricalsDetailComponent } from './components/historicals-detail/historicals-detail.component';
import { HistoricalsCreateComponent } from './components/historicals-create/historicals-create.component';
import { HistoricalsListController } from './components/historicals-list/historicals-list.controller';
import { HistoricalResource } from './resources/historicals.resource';
import { HistoricalsDetailController } from './components/historicals-detail/historicals-detail.controller';
import { HistoricarslCreateController } from './components/historicals-create/historicals-create.controller';

@NgModule({
    declarations: [
        HistoricalsListComponent,
        HistoricalsDetailComponent,
        HistoricalsCreateComponent
    ],
    imports: [
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        MaterialModule
    ],
    providers: [
        HistoricalsListController,
        HistoricalResource,
        HistoricalsDetailController,
        HistoricarslCreateController
    ]
})

export class HistoricalsModule {

}