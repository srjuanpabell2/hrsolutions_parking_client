import { VehicleType } from "../../Vehicles/models/vehicle_type.model";
import { Vehicle } from "../../Vehicles/models/vehicle.model";

export class Historical {

    public id: number;
    public comments: string;
    public vehicle: Vehicle;
    public created_at: string;
    public updated_at: string;
    public start_time: string;
    public end_time: string;
    public total_value: number;
    public vehicle_id: number;

    constructor(obj?: any) {
        if (obj) {
            this.id = obj.id;
            this.comments = obj.comments;
            this.created_at = obj.created_at;
            this.vehicle_id = obj.vehicle_id;
            this.total_value = obj.total_value;
            this.updated_at = obj.updated_at;
            this.start_time = obj.start_time;
            this.end_time = obj.end_time;
            this.vehicle = new Vehicle(obj.vehicle);
        }
    }

}