import { Injectable } from "@angular/core";
import { VehicleTypesListComponent } from "./vehicle-types-list.component";
import { VehicleTypesResource } from "../../resources/vehicles-types.resource";

@Injectable()

export class VehicleTypesListController {

    component: VehicleTypesListComponent;

    constructor(private VehicleTypesResource: VehicleTypesResource) { }

    setComponent(component: VehicleTypesListComponent) {
        this.component = component;
    }

    getVehicleTypes() {
        this.VehicleTypesResource.setEntity('vehicle_types');
        this.VehicleTypesResource.all().subscribe((vehicle_types: any) => {
            this.component.setVehicleTypes(vehicle_types.data);
        }, (err) => {
            console.log(err.error.message);
        });
    }

}