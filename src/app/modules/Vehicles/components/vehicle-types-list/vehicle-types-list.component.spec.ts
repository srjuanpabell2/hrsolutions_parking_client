import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleTypesListComponent } from './vehicle-types-list.component';

describe('VehicleTypesListComponent', () => {
  let component: VehicleTypesListComponent;
  let fixture: ComponentFixture<VehicleTypesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleTypesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleTypesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
