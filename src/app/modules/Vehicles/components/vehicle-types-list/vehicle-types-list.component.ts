import { Component, OnInit } from '@angular/core';
import { VehicleTypesListController } from './vehicle-types-list.controller';
import { VehicleType } from '../../models/vehicle_type.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-vehicle-types-list',
  templateUrl: './vehicle-types-list.component.html',
  styleUrls: ['./vehicle-types-list.component.css']
})
export class VehicleTypesListComponent implements OnInit {

  private vehicle_types: VehicleType[] = [];

  constructor(private controller: VehicleTypesListController,
              private router: Router) { 
                this.controller.setComponent(this);
  }

  ngOnInit() {
    this.controller.getVehicleTypes();
  }
  
  getVehicleTypes() {
    return this.vehicle_types;
  }

  setVehicleTypes(vehicle_types: VehicleType[]) {
    this.vehicle_types = vehicle_types;
  }

  onGoToDeleteTypes(vehicle_type_id: number) {
    this.router.navigateByUrl('vehicle_types_delete/' + vehicle_type_id);
  }

  onGoToEditVehicleTypes(vehicle_type_id: number) {
    this.router.navigateByUrl('vehicle_types_edit/' + vehicle_type_id);
  }

}
