import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { VehicleCreateController } from './vehicle-create.controller';
import { VehicleType } from '../../models/vehicle_type.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastsManager } from 'ng2-toastr'

@Component({
  selector: 'app-vehicle-create',
  templateUrl: './vehicle-create.component.html',
  styleUrls: ['./vehicle-create.component.css']
})
export class VehicleCreateComponent implements OnInit {

  private vehicleTypes: VehicleType[] = [];
  createForm: FormGroup;

  constructor(private controller: VehicleCreateController,
              private router: Router,
              private route: ActivatedRoute,
              private toastr: ToastsManager,
              private vcr: ViewContainerRef
            ) { 
                this.controller.setComponent(this);
                this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.controller.getVehicleTypes();
    this.createForm = new FormGroup({
      identification: new FormControl(null, [Validators.required]),
      vehicle_type_id: new FormControl(null, [Validators.required])
    })
  }

  getVehicleTypes() {
    return this.vehicleTypes;
  }

  getCreateForm() {
    return this.createForm;
  }

  setVehicleTypes(vehicle_types: VehicleType[]) {
    this.vehicleTypes = vehicle_types;
  }

  showError(msg) {
    this.toastr.error(msg);
  }

  showSuccess(msg) {
    this.toastr.success(msg);
  }

  onCreate() {
    if (this.createForm.valid) {
      this.controller.onCreate();
    }
  }

  onGoVehicles() {
    this.router.navigateByUrl('vehicles');
  }

}
