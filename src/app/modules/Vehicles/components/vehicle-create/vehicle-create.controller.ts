import { Injectable } from "@angular/core";
import { VehicleCreateComponent } from "./vehicle-create.component";
import { VehicleTypesResource } from "../../resources/vehicles-types.resource";
import { VehiclesResource } from "../../resources/vehicles.resource";

@Injectable()

export class VehicleCreateController {

    component: VehicleCreateComponent;

    constructor(private vehicleTypesResource: VehicleTypesResource,
                private vehiclesResource: VehiclesResource) { }

    setComponent(component: VehicleCreateComponent) {
        this.component = component;
    }

    getVehicleTypes() {
        this.vehicleTypesResource.setEntity('vehicle_types');
        this.vehicleTypesResource.all().subscribe((vehicle_types: any) => {
            this.component.setVehicleTypes(vehicle_types.data);
        }, (err) => {
            console.log(err.error.message);
        });
    }

    onCreate() {
        this.vehiclesResource.setEntity('vehicles');
            this.vehiclesResource.store({
                identification: this.component.getCreateForm().value.identification,
                vehicle_type_id: Number(this.component.getCreateForm().value.vehicle_type_id)
            }).subscribe(() => {
                this.component.showSuccess('El vehiculo se ha creado correctamente');
                setTimeout(() => {
                    this.component.onGoVehicles();
                }, 2000);
            }, (err) => {
                console.log(err.error.message);
            });
    }

}