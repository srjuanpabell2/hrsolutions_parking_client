import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleTypesDeleteComponent } from './vehicle-types-delete.component';

describe('VehicleTypesDeleteComponent', () => {
  let component: VehicleTypesDeleteComponent;
  let fixture: ComponentFixture<VehicleTypesDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleTypesDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleTypesDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
