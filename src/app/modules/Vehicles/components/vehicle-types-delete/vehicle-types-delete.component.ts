import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { VehicleTypesDeleteController } from './vehicle-types-delete.controller';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';

@Component({
  selector: 'app-vehicle-types-delete',
  templateUrl: './vehicle-types-delete.component.html',
  styleUrls: ['./vehicle-types-delete.component.css']
})
export class VehicleTypesDeleteComponent implements OnInit {

  constructor(private controller: VehicleTypesDeleteController,
              private router: Router,
              private route: ActivatedRoute,
              private toastr: ToastsManager,
            private vcr: ViewContainerRef) { 
                this.controller.setComponent(this);
                this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
  }

  showError(msg) {
    this.toastr.error(msg);
  }

  showSuccess(msg) {
    this.toastr.success(msg);
  }

  onDelete() {
    this.controller.onDelete(this.route.snapshot.params['vehicle_type_id']);
  }

  onCancel() {
    this.router.navigateByUrl('vehicle_types');
  }

}
