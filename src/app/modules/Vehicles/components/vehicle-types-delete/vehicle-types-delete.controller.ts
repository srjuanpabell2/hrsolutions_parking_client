import { Injectable } from "@angular/core";
import { VehicleTypesDeleteComponent } from "./vehicle-types-delete.component";
import { VehicleTypesResource } from "../../resources/vehicles-types.resource";

@Injectable()

export class VehicleTypesDeleteController {

    component: VehicleTypesDeleteComponent;

    constructor(private vehicleTypesDeleteResource: VehicleTypesResource) { }

    setComponent(component: VehicleTypesDeleteComponent) {
        this.component = component;
    }

    onDelete(vehicle_type_id: number) {
        this.vehicleTypesDeleteResource.setEntity('vehicle_types');
        this.vehicleTypesDeleteResource.delete(vehicle_type_id).subscribe(() => {
            this.component.showError('El tipo de vehículo se ha eliminado correctamente');
            setTimeout(() => {
                this.component.onCancel();
            }, 2000);
        }, (err) => {
            console.log(err.error.message);
        });
    }

}