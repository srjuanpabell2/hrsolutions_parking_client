import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { VehiclesTypesCreateController } from './vehicle-types-create.controller';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';

@Component({
  selector: 'app-vehicle-types-create',
  templateUrl: './vehicle-types-create.component.html',
  styleUrls: ['./vehicle-types-create.component.css']
})
export class VehicleTypesCreateComponent implements OnInit {

  createForm: FormGroup;

  constructor(private controller: VehiclesTypesCreateController,
              private router: Router,
              private toastr: ToastsManager,
              private vcr: ViewContainerRef) { 
                this.controller.setComponent(this);
                this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.createForm = new FormGroup({
      name: new FormControl(null, [Validators.required])
    });
  }

  onCreate() {
    if (this.createForm.valid) {
      this.controller.onCreate();
    }
  }

  onGoVehicleTypes() {
    setTimeout(() => {
      this.router.navigateByUrl('vehicle_types');  
    }, 2000);
    this.toastr.success('Tipo de vehículo ' + this.createForm.value.name + ' creado correctamente');
  }

  getForm() {
    return this.createForm;
  }

  showSuccess(msg) {
    this.toastr.success(msg);
  }

  showError(msg) {
    this.toastr.error(msg);
  }

}
