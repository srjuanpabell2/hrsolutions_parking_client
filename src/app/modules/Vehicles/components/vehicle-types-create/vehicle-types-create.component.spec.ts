import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleTypesCreateComponent } from './vehicle-types-create.component';

describe('VehicleTypesCreateComponent', () => {
  let component: VehicleTypesCreateComponent;
  let fixture: ComponentFixture<VehicleTypesCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleTypesCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleTypesCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
