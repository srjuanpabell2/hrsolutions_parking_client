import { Injectable } from "@angular/core";
import { VehicleTypesCreateComponent } from "./vehicle-types-create.component";
import { VehicleTypesResource } from "../../resources/vehicles-types.resource";
import { VehiclesResource } from "../../resources/vehicles.resource";

@Injectable()

export class VehiclesTypesCreateController {

    component: VehicleTypesCreateComponent;

    constructor(private vehicleTypesResource: VehicleTypesResource,
        private vehicleResource: VehiclesResource) { }

    setComponent(component: VehicleTypesCreateComponent) {
        this.component = component;
    }

    onCreate() {
        var form = this.component.getForm();
        this.vehicleTypesResource.setEntity('vehicle_types');
        this.vehicleTypesResource.store(form.value).subscribe(() => {
            this.component.onGoVehicleTypes();
        }, (err) => {
            console.log(err.error.message);
        })
    }

}