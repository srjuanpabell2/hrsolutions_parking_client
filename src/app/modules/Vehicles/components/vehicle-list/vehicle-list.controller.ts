import { Injectable } from "@angular/core";
import { VehicleListComponent } from "./vehicle-list.component";
import { VehiclesResource } from "../../resources/vehicles.resource";

@Injectable()

export class VehicleListController {

    component: VehicleListComponent;

    constructor(private vehiclesResource: VehiclesResource) { }

    setComponent(component: VehicleListComponent) {
        this.component = component;
    }

    getVehicles() {
        this.vehiclesResource.setEntity('vehicles');
        this.vehiclesResource.all().subscribe((vehicles: any) => {
            this.component.setVehicles(vehicles.data.data);
        }, (err) => {
            console.log(err.error.message);
        })
    }

}