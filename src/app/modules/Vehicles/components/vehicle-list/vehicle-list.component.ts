import { Component, OnInit } from '@angular/core';
import { VehicleListController } from './vehicle-list.controller';
import { Vehicle } from '../../models/vehicle.model';

@Component({
  selector: 'app-vehicle-list',
  templateUrl: './vehicle-list.component.html',
  styleUrls: ['./vehicle-list.component.css']
})
export class VehicleListComponent implements OnInit {

  private vehicles: Vehicle[];

  constructor(private controller: VehicleListController) { 
                this.controller.setComponent(this);
  }

  ngOnInit() {
    this.controller.getVehicles();
  }

  getVehicles() {
    return this.vehicles;
  }

  setVehicles(vehicles: Vehicle[]) {
    this.vehicles = vehicles;
  }
}
