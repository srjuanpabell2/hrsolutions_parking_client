import { VehicleType } from "./vehicle_type.model";

export class Vehicle {

    public id: number;
    public identification: string;
    public vehicle_type: VehicleType;
    public created_at: Date;

    constructor(obj?: any) {
        if (obj) {
            this.id = obj.id;
            this.identification = obj.identification;
            this.vehicle_type = new VehicleType(obj.vehicle_type);
            this.created_at = new Date(obj.created_at);
        }
    }



}