import { Rate } from "../../Rates/models/rate.model";

export class VehicleType {

    public id: number;
    public name: string;
    public created_at: Date;
    public rate: Rate;

    constructor(obj?: any) {
        if (obj) {
            this.id = obj.id;
            this.name = obj.name;
            this.created_at = new Date(obj.created_at);
            if (obj.rate) {
                this.rate = new Rate(obj.rate);
            }
        } 
    }

}