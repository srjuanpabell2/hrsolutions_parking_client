import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../app.material.module';
import { VehicleTypesCreateComponent } from './components/vehicle-types-create/vehicle-types-create.component';
import { VehiclesTypesCreateController } from './components/vehicle-types-create/vehicle-types-create.controller';
import { VehiclesResource } from './resources/vehicles.resource';
import { VehicleTypesResource } from './resources/vehicles-types.resource';
import { VehicleTypesListComponent } from './components/vehicle-types-list/vehicle-types-list.component';
import { VehicleListComponent } from './components/vehicle-list/vehicle-list.component';
import { VehicleCreateComponent } from './components/vehicle-create/vehicle-create.component';
import { VehicleTypesListController } from './components/vehicle-types-list/vehicle-types-list.controller';
import { VehicleTypesDeleteComponent } from './components/vehicle-types-delete/vehicle-types-delete.component';
import { VehicleTypesDeleteController } from './components/vehicle-types-delete/vehicle-types-delete.controller';
import { VehicleCreateController } from './components/vehicle-create/vehicle-create.controller';
import { VehicleListController } from './components/vehicle-list/vehicle-list.controller';

@NgModule({
    declarations: [
        VehicleTypesCreateComponent,
        VehicleTypesListComponent,
        VehicleListComponent,
        VehicleCreateComponent,  
        VehicleTypesDeleteComponent
    ],
    imports: [
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        MaterialModule
    ],
    providers: [
        VehiclesTypesCreateController,
        VehiclesResource,
        VehicleTypesResource,
        VehicleTypesListController,
        VehicleTypesDeleteController,
        VehicleCreateController,
        VehicleListController
    ]
})

export class VehiclesModule {

}