import { Injectable } from "@angular/core";
import { Resource } from "../../General/resources/base.resource";
import { Vehicle } from "../models/vehicle.model";

@Injectable()

export class VehiclesResource extends Resource {

    protected map(data) {
        return new Vehicle(data);
    }

}