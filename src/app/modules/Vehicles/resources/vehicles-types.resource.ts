import { Injectable } from "@angular/core";
import { Resource } from "../../General/resources/base.resource";
import { VehicleType } from "../models/vehicle_type.model";


@Injectable()

export class VehicleTypesResource extends Resource {

    protected map(data) {
        return new VehicleType(data);
    }

}