import { Injectable } from '@angular/core';
import { SERVER_URL } from '../../../app-config';
import 'rxjs/Rx';
import { HttpClient } from '@angular/common/http';
import { Filter } from '../services/filter.service';

@Injectable()
export abstract class Resource {

  protected paginateInfo: any = {};
  protected entity: string = null;
  protected filters: Filter[] = [];
  protected queryFilters = '';

  constructor(protected http: HttpClient) { }

  all(page?: number) {
    let pageFilter = page ? '&page= ' + page.toString() : '';
    return this.http.get(SERVER_URL + this.entity + pageFilter + this.queryFilters);
  }

  get(id) {
    return this.http.get(SERVER_URL + this.entity + '/' + id);
  }

  store(data) {
    return this.http.post(SERVER_URL + this.entity, data);
  }

  update(id, data) {
    return this.http.put(SERVER_URL + this.entity + '/' + id, data);
  }

  delete(id) {
    return this.http.delete(SERVER_URL + this.entity + '/' + id);
  }

  setEntity(entity: string) {
    this.entity = entity;
  }

  public addFilter(variable: string, condition: string, value: string) {
    this.filters.push(new Filter(variable, condition, value));
  }

  public removeFilter(filter: Filter) {
    this.filters.splice(this.filters.indexOf(filter), 1);
  }

  public clearFilters() {
    this.filters = [];
    this.queryFilters = '';
  }

  public applyQueryFilters() {
    let queryFilters: string [] = [];
    for (let filter of this.filters) {
      queryFilters.push(filter.toQueryString());
    }
    if (queryFilters.length > 0) {
      this.queryFilters = '&filters=' + queryFilters.join(';');
    }
  }

  public getPaginateInfo() {
    return this.paginateInfo;
  }

  private mapArray(data) {
    if (data.total) {
      this.paginateInfo.current_page = data.current_page;
      this.paginateInfo.from = data.from;
      this.paginateInfo.last_page = data.last_page;
      this.paginateInfo.per_page = data.per_page;
      this.paginateInfo.to = data.to;
      this.paginateInfo.total = data.total; 
      data = data.data;
    }
    let array = [];
    for (let item of data) {
      array.push(this.map(item));
    }
    return array;
  }

  protected abstract map(data);

}
