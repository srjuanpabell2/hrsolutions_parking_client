export class StorageService {
    
    write(key: string, value: any) {
      if (value) {
        value = JSON.stringify(value);
      }
      localStorage.setItem(key, value);
    }
  
    read(key: string): string {
      let value: string = localStorage.getItem(key);
      if (value && value != 'undefined' && value != 'null') {
        return value;
      }
      return null;
    }
    
    delete(key: string) {
      localStorage.removeItem(key);
    }
  
  }