export class Filter {
    constructor(public variable: string,
                public condition: string,
                public value: string) {}

    public toQueryString(): string {
      return this.variable + '|' + this.condition + '|' + this.value;
    }
}