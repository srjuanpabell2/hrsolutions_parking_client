import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../app.material.module';
import { NavigationComponent } from './components/navigation/navigation.component';
import { StorageService } from './services/storage.service';

@NgModule({
    declarations: [
        NavigationComponent
    ],
    imports: [
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        MaterialModule
    ],
    providers: [
        StorageService
    ],
    exports: [
        NavigationComponent
    ]
})

export class GeneralModule {

}