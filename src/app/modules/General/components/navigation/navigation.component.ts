import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StorageService } from '../../services/storage.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  constructor(private router: Router,
              private storageService: StorageService) { }

  ngOnInit() {
  }

  onGoToCreateVehicleType() {
    this.router.navigateByUrl('vehicle_types_create');
  }

  onGoToHistoricals() {
    this.router.navigateByUrl('historicals');
  }

  onGoToCreateVehicle() {
    this.router.navigateByUrl('vehicle_create');
  }

  onGoToCreateRate() {
    this.router.navigateByUrl('rates_create');
  }

  onGoToVehicles() {
    this.router.navigateByUrl('vehicles');
  }

  onGoToRates() {
    this.router.navigateByUrl('rates');
  }

  onGoToCreateHistorical() {
    this.router.navigateByUrl('historical_create');
  }

}
