import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../app.material.module';
import { RatesCreateComponent } from './components/rates-create/rates-create.component';
import { RatesCreateController } from './components/rates-create/rates-create.controller';
import { RatesResource } from './resources/rates.resource';

@NgModule({
    declarations: [
        RatesCreateComponent
    ],
    imports: [
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        MaterialModule
    ],
    providers: [
        RatesCreateController,
        RatesResource
    ]
})

export class RatesModule {

}