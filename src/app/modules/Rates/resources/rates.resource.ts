import { Injectable } from "@angular/core";
import { Resource } from "../../General/resources/base.resource";
import { Rate } from "../models/rate.model";

@Injectable()

export class RatesResource extends Resource {

    protected map(data) {
        return new Rate(data);
    }

}