import { Injectable } from "@angular/core";
import { RatesCreateComponent } from "./rates-create.component";
import { RatesResource } from "../../resources/rates.resource";
import { VehicleTypesResource } from "../../../Vehicles/resources/vehicles-types.resource";
import { VehicleType } from "../../../Vehicles/models/vehicle_type.model";

@Injectable()

export class RatesCreateController {

    component: RatesCreateComponent;
    vehicle_types: VehicleType[] = new Array<VehicleType>();

    constructor(private ratesResource: RatesResource,
                private vehicleTypesResource: VehicleTypesResource) { }

    setComponent(component: RatesCreateComponent) {
        this.component = component;
    }

    getVehicleTypes() {
        this.vehicleTypesResource.setEntity('vehicle_types');
        this.vehicleTypesResource.clearFilters();
        this.vehicleTypesResource.addFilter('rate.minute_value', '=', '0');
        this.vehicleTypesResource.all().subscribe((vehicle_types: any) => {
            let x = this;
            for(let vehicle_type of vehicle_types.data) {
                if (vehicle_type.rate == null) {
                    x.vehicle_types.push(vehicle_type);
                }
            }
            this.component.setVehicleTypes(x.vehicle_types);
        }, (err) => {
            console.log(err.error.message);
        });
    }

    onCreate() {
        this.ratesResource.setEntity('rates');
        this.ratesResource.store({
            minute_value: Number(this.component.getCreateForm().value.minute_value),
            vehicle_type_id: Number(this.component.getCreateForm().value.vehicle_type_id)
        }).subscribe(() => {
            this.component.showSuccess('La tarifa se ha creado correctamente');
            setTimeout(() => {
                this.component.onGoRates();
            }, 2000);
        }, (err) => {
            console.log(err.error.message);
        });
    }
}