import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RatesCreateComponent } from './rates-create.component';

describe('RatesCreateComponent', () => {
  let component: RatesCreateComponent;
  let fixture: ComponentFixture<RatesCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RatesCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RatesCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
