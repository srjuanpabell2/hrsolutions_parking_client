import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { RatesCreateController } from './rates-create.controller';
import { VehicleType } from '../../../Vehicles/models/vehicle_type.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastsManager } from 'ng2-toastr';

@Component({
  selector: 'app-rates-create',
  templateUrl: './rates-create.component.html',
  styleUrls: ['./rates-create.component.css']
})
export class RatesCreateComponent implements OnInit {

  private vehicleTypes: VehicleType[];
  createForm: FormGroup;

  constructor(private controller: RatesCreateController,
              private router: Router,
              private route: ActivatedRoute,
              private toastr: ToastsManager,
              private vcr: ViewContainerRef) { 
                this.controller.setComponent(this);
                this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.controller.getVehicleTypes();
    this.createForm = new FormGroup({
      minute_value: new FormControl(null, [Validators.required]),
      vehicle_type_id: new FormControl(null, [Validators.required])
    })
  }

  getVehicleTypes() {
    return this.vehicleTypes;
  }

  getCreateForm() {
    return this.createForm;
  }

  setVehicleTypes(vehicle_types: VehicleType[]) {
    this.vehicleTypes = vehicle_types;
  }

  showError(msg) {
    this.toastr.error(msg);
  }

  showSuccess(msg) {
    this.toastr.success(msg);
  }

  onCreate() {
    if (this.createForm.valid) {
      this.controller.onCreate();
    }
  }

  onGoRates() {
    this.router.navigateByUrl('vehicle_types');
  }

}
