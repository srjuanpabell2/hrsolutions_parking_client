export class Rate {

    public id: number;
    public vehicle_type_id: number;
    public minute_value: number;
    public created_at: Date;
    
    constructor(obj?: any) {
        if (obj) {
            this.id = obj.id;
            this.vehicle_type_id = obj.vehicle_type_id;
            this.minute_value = obj.minute_value;
            this.created_at = new Date(obj.created_at);
        }
    }

}