import { NgModule } from '@angular/core';
import { MatToolbarModule,
         MatIconModule,
         MatCardModule,
         MatInputModule,
         MatButtonModule,
         MatFormFieldModule,
         MatListModule,
         MatGridListModule,
         MatRadioModule
        } from '@angular/material';

const MODULES = [
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    MatListModule,
    MatGridListModule,
    MatRadioModule
];

@NgModule({
    imports: MODULES,
    exports: MODULES
}

)
export class MaterialModule { }